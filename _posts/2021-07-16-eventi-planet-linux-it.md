---
layout: post
title: Eventi su Planet.Linux.it
image: /assets/posts/images/planet.png
---

[Planet.Linux.it](https://planet.linux.it/), l'aggregatore delle news del mondo opensource in Italia, è stato esteso per aggregare in modo strutturato anche gli eventi organizzati dalle diverse community locali all'interno di [un calendario unico](https://planet.linux.it/eventi/).

<!--more-->

Lo strumento raccoglie periodicamente informazioni estratte da calendari esterni in formato ICS (adottato da molti CMS e plugin per la pubblicazione di eventi e da numerose piattaforme dedicate alla gestione degli appuntamenti tra cui [Mobilizon](https://joinmobilizon.org/it/)), in formato CalDAV (usato ad esempio dalla app calendar di [NextCloud](https://nextcloud.com/)) e da [Meetup.com](https://www.meetup.com/) (altra piattaforma molto popolare, benché non libera) e le riorganizza in un'unica pagina, permettendo di scoprire a colpo d'occhio tutti gli appuntamenti a disposizione nel prossimo futuro. Inclusi i numerosi incontri online che, a prescindere dal gruppo organizzatore, sono fruibili da tutti.

L'elenco dei calendari coinvolti conta al momento circa 70 realtà in tutta Italia, ed è naturalmente possibile fare richiesta per aggiungere anche la propria sorgente. Agendo [direttamente sul repository](https://gitlab.com/ItalianLinuxSociety/planet/-/blob/master/eventi/calendars.txt), con una pull request, oppure mandando una email all'indirizzo webmaster@linux.it.
