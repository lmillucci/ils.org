---
layout: post
title: Vetrine LinuxSi
created: 1472935304
---
<img src="/assets/posts/images/adesivo_linuxsi.jpg" style="margin: auto; display: block" />

<br/>

<p>
Da diversi anni indicizziamo su <a href="http://www.linuxsi.com/">LinuxSi</a> i negozi di informatica Linux-friendly, presso cui è possibile acquistare computer con Linux o senza nessun sistema operativo pre-installato e grazie ai quali dunque si può evitare di pagare la "tassa Microsoft", ovvero il costo di una nuova licenza Windows che grava, spesso in modo mascherato, sul prezzo di gran parte dei dispositivi disponibili sul mercato (costo che, ricordiamo sempre, <a href="http://sistemainoperativo.it/">può e deve essere rimborsabile</a>).
</p>

<p>
Oggi vogliamo fare qualcosa in più per coloro che offrono questo importante servizio, e che con la propria attività agevolano l'adozione di Linux anche presso gli utenti meno esperti: abbiamo realizzato degli adesivi destinati alle vetrine di suddetti negozi, per meglio segnalare la possibilità di ricevere lì supporto su PC Linux, e, magari, incuriosire coloro che passano davanti a tale emblema e fermarsi per chiedere maggiori delucidazioni.
</p>

<p>
La prima serie è stata destinata ad alcuni negozi già indicizzati sul sito LinuxSi, ma ne abbiamo altri da spedire (gratuitamente, senza alcuna spesa) a tutti coloro che vogliono aderire all'iniziativa e che, anche se non offrono computer con Linux pre-installato, sono comunque in grado di fornire assistenza e vogliono segnalarlo in modo più evidente ai propri clienti (attuali o futuri). Per farne domanda è sufficiente <a href="http://www.linuxsi.com/partecipa/">contattarci</a> e fornirci un indirizzo fisico per la spedizione.
</p>

<p>
Gli adesivi sono rotondi, 7 centimetri di diametro, bianchi plastificati per esterni, poco invasivi ma comunque visibili.
</p>

<p>
Grazie a coloro che, anche con la propria attività professionale, contribuiscono a diffondere Linux ed il software libero ad un numero sempre maggiore di persone!
</p>
