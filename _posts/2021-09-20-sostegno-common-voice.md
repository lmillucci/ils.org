---
layout: post
title: Sostegno a Common Voice
image: /assets/posts/images/commonvoice.jpeg
---

Il progetto [Common Voice di Mozilla](https://commonvoice.mozilla.org/) ha l'ambizioso obiettivo di realizzare modelli vocali - da distribuire in licenza libera e aperta - in numerose lingue del mondo, e per farlo raccoglie ed elabora i contributi di migliaia di volontari che "donano" la propria voce. E ILS ha voluto donare anche qualcos'altro.

<!--more-->

Al fine di rendere sempre più facile e comodo contribuire al progetto, la cui base di dati viene alimentata dai volontari che registrano la propria voce mentre leggono alcune frasi predefinite e convalidano quelle registrate dagli altri (arricchendo dunque la collezione di accoppiamenti testo/voce indispensabile per estrapolare poi un modello generico), Italian Linux Society ha riconosciuto a Saverio Morelli - autore della app [CV Project](https://www.saveriomorelli.com/commonvoice/), la più popolare relativa a Common Voice, usata dai partecipanti di ogni continente e di ogni lingua - un **contributo di 2000 euro** come ringraziamento per il lavoro svolto sinora e incoraggiamento a mantenerlo nel tempo.

Confidiamo che il bacino di volontari che vorranno contribuire - sul sito del progetto Common Voice, o appunto per mezzo della app mobile - possa aumentare progressivamente, e di poter presto avere un modello vocale in lingua italiana libero e gratuito che possa essere usato per le più disparate applicazioni. In primis quelle di *text-to-speech* e *speech-to-text*, utili a garantire l'accessibilità delle applicazioni da parte delle persone con disabilità.
