---
layout: post
title: Pillole di WiiLDOS
created: 1417390393
---
La mailing list <a rel="nofollow" href="https://groups.google.com/forum/#!forum/wii_libera_la_lavagna">"wii libera la lavagna"</a> è la più importante community italiana di discussione sul tema del software libero a scuola, ma non solo: sulla lista transitano quotidianamente segnalazioni di risorse didattiche liberamente fruibili online, annunci di applicazioni libere, notizie di attualità, richieste di supporto tecnico, commenti ed opinioni. Certamente il luogo (virtuale) ideale dove reperire competenze specifiche sull'argomento, ed incontrare tante persone appassionate ed interessanti.

La sua unica debolezza è proprio il suo stesso successo: seguire le decine di mail che ogni giorno vengono inviate al gruppo non è sempre semplice, soprattutto per chi è meno avvezzo alla mailing list - da sempre, canale preferenziale di pressoché ogni comunità opensource - come strumento di scambio e condivisione. Con l'intento di rendere più morbido l'impatto con questa dinamica e vivace realtà da oggi Italian Linux Society introduce nella sua newsletter mensile la rubrica "Pillole di WiiLDOS", con un riassunto dei messaggi più rilevanti ed interessanti passati nei precedenti 30 giorni: un modo per dare risalto e visibilità alle questioni di maggiore impatto, esponendole agli oltre 2700 iscritti alla newsletter ILS, pur evitando di dover leggere e filtrare centinaia di messaggi.

Chiaramente consigliamo comunque a tutti di iscriversi alla mailing list "wii libera la lavagna", per poter partecipare attivamente, poter a propria volta segnalare contenuti interessanti o chiedere supporto, e non perdere nessuna delle discussioni di passaggio.

E' possibile iscriversi alla newsletter mensile (e contestualmente agli annunci degli <a href="http://lugmap.linux.it/eventi/">eventi territoriali</a>) su <a href="/newsletter">questa pagina</a>.
