---
layout: post
title: 'La Libertà non ha Prezzo: Progetto So.Di.Linux'
created: 1463694648
---
<p style="text-align: center">
<img class="img-fluid" src="/assets/posts/images/donazioni_1.png" />
</p>

<p>
Annunciamo oggi la quarta campagna di raccolta fondi su <a href="http://donazioni.linux.it/">donazioni.linux.it</a>!
</p>

<p>
Il soggetto di questa nuova iterazione è <a rel="nofollow" href="http://sodilinux.itd.cnr.it/">So.Di.Linux</a>, storica distribuzione Linux dedicata alla scuola, uno dei progetti più longevi - e, nonostante ciò, attivi - del panorama freesoftware italiano. Il sistema è dotato di numerosi pacchetti software, tutti liberi ed aperti, utili per la didattica nelle scuole di ogni ordine e grado, supporta nativamente l'interazione sulle Lavagne Interattive Multimediali, ed è supportato da una vasta community distribuita in tutta Italia. Inoltre una delle caratteristiche più importanti della piattaforma è l'attenzione posta nei confronti delle disabilità e la disponibilità di strumenti mirati ad affrontare i cosiddetti "bisogni educativi speciali".
</p>

<p>
Gran parte dei fondi che raccoglieremo sarà destinata direttamente al mantenimento ed all'aggiornamento della distribuzione, ma sono previste altre attività parallele e trasversali. A partire dal progetto di adattare i pacchetti software appositamente realizzati per So.Di.Linux anche ad altre distribuzioni basate su Ubuntu, e permettere dunque ad un bacino ancora più esteso di utenti di installarli agevolmente in altri contesti. E poi, l'intento di sostenere lo sviluppo di programmi strategici non solo per So.Di.Linux ma per tutto il software libero nella scuola, tra cui il <a rel="nofollow" href="http://www.openlabasti.it/arasuiteita/">progetto Araword</a>, importante applicazione per la "comunicazione aumentativa".
</p>

<p>
Anche per il progetto So.Di.Linux i soci di Italian Linux Society mettono a disposizione i primi 3000 euro, e tutte le quote delle nuove <a href="/iscrizione">iscrizioni all'associazione</a> che arriveranno nel corso della campagna saranno immediatamente destinate a questo fondo.
</p>
