---
layout: post
title: Di Nuovo Online
created: 1364289640
---
Italian Linux Society, associazione nazionale di promozione e supporto a Linux ed al software libero, rinnova la sua presenza sul web con due nuovi siti: <a href="/">ILS.org</a> e <a href="http://www.linux.it/">Linux.it</a>.

Il primo, destinato soprattutto a chi già conosce il mondo del software libero, offre una carrellata dei servizi e delle opportunità offerte da Italian Linux Society nei confronti dei numerosi Linux User Group che popolano ed animano il nostro Paese e nei confronti degli interessati al tema che vogliono in qualche modo contribuire. Qui si trovano news, aggiornamenti, e documentazione utile per chi fa parte (o vuole far parte) della community freesoftware italiana.

Il secondo ha l'ambizione di essere una risorsa primaria per coloro che hanno sentito parlare di Linux ma vogliono saperne qualcosa di più, non una destinazione ma un punto di partenza per meglio comprendere e capire sia le definizioni tecniche che le motivazioni di un movimento, quello freesoftware, che ha cambiato e sta continuando a cambiare il mondo. Da qui è possibile farsi una idea del come e del perché di Linux, ma soprattutto individuare il più vicino gruppo di appassionati locale presso cui rivolgersi e partecipare.

La nuova piattaforma si estende oltre che sul web anche sul canale mail: tutti gli utenti di passaggio possono sottoscrivere la newsletter non periodica di Italian Linux Society, per ricevere direttamente nella casella di posta elettronica aggiornamenti sul panorama freesoftware e notifiche puntuali sulle attività che si svolgono nella loro zona di residenza in modo da essere sempre informati ed essere coinvolti ancora più attivamente.

I nuovi ILS.org e Linux.it fanno seguito ai precedenti redesign di <a href="http://lugmap.linux.it/">LugMap.Linux.it</a>, l'indice dei Linux User Group italiani, e di <a href="http://linuxday.it/">LinuxDay.it</a>, sito di riferimento della maggiore manifestazione di promozione e sensibilizzazione al software libero in Italia.
