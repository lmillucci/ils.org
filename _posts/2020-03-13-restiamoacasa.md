---
layout: post
title: "#RestiamoACasa"
created: 1584120727
---

Tutti i cittadini italiani sono invitati in questi giorni a limitare gli spostamenti, per evitare l'ulteriore diffusione del virus COVID-19. Scuole ed uffici sono chiusi, ma anche locali e ristoranti, e l'unica opzione è restare a casa ed ingegnarsi per passare il tempo.

<!--more-->

Un modo intelligente di farlo è quello di contribuire all'arricchimento della conoscenza libera, approfittando di questa condizione per partecipare ad una delle tante iniziative (rigorosamente online) destinate ad accrescere, migliorare e perfezionare il patrimonio culturale (digitale, ma non solo) a disposizione di tutti.

Una serie di spunti operativi sono disponibili <a href="https://www.linux.it/todo">su questa pagina</a>, che raccoglie le idee periodicamente pubblicate nella rubrica "TODO" della <a href="/newsletter">newsletter di Italian Linux Society</a>, ma in particolare segnaliamo:

<ul>
<li><a rel="nofollow" href="https://www.wikimedia.it/una-maratona-di-scrittura-per-litalia-la-comunita-di-wikipedia-si-attiva/">l'appello lanciato da Wikimedia Italia</a> per contribuire a Wikipedia, l'enciclopedia libera</li>
<li>il progetto <a rel="nofollow" href="https://voice.mozilla.org/it">Common Voice</a> promosso da Mozilla, cui partecipare recitando e registrando brevi testi da usare per la creazione di un modello vocale italiano libero (utilissimo per potenziare le applicazioni di speech-to-text e text-to-speech nella nostra lingua)</li>
<li><a rel="nofollow" href="https://hosted.weblate.org/projects/">Weblate</a>, su cui sono ospitati centinaia di progetti open source in attesa di traduzione (in italiano, ma non solo)</li>
</ul>
