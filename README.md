# Sito ILS

Questo è il repository del sito https://www.ils.org generato con [Jekyll](https://jekyllrb.com/).

## Sviluppo

Esplora il repository, è strutturato in maniera molto semplice.

In fase di sviluppo è possibile vedere in tempo reale le modifiche, eseguendo:

```
bundle exec jekyll serve --livereload
```

## Modifiche

Una volta che hai terminato le tue modifiche, fai un commit.

Se non l'hai ancora fatto, per favore esegui questo comando:

```
JEKYLL_ENV=production bundle exec jekyll build
```

In questo modo viene ricompilato l'intero sito. Aggiungi tutti i file compilati e fai un altro commit.

Attenzione: questo incorpora nelle pagine dei post il widget per i commenti Discourse.

Se non fai questa cosa, la facciamo noi, non preoccuparti. Al momento è necessario perchè il server web
attualmente in uso fa semplicemente "git pull" dal master per l'aggiornamento, e non ricompila i file.

Un giorno adotteremo un processo più moderno :D chissà quando.

## Segnalazioni

Per segnalazioni e richieste, scrivi a:

webmaster@linux.it

## Licenza

Salvo ove diversamente indicato tutti i contenuti sono rilasciati in pubblico dominio, Creative Commons Zero.

https://creativecommons.org/publicdomain/zero/1.0/
