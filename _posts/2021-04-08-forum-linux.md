---
layout: post
title: Forum.Linux.IT
image: /assets/posts/images/forum.png
---

Da sempre Italian Linux Society hosta e mantiene per i propri soci - ed in particolare per i Linux Users Groups - mailing list di varia natura per permettere alle realtà locali di restare in contatto, scambiarsi segnalazioni, organizzare iniziative e crescere. Ma i tempi cambiano, e così cambiano anche gli strumenti di comunicazione.

<!--more-->

Nasce così [forum.linux.it](https://forum.linux.it/), destinato a far convergere alcuni servizi e soprattutto a fungere da alternativa alle vecchie mailing list (modalità non sempre apprezzata e compresa, in particolare dalle nuove generazioni) o ai gruppi Facebook che in molti purtroppo adottano (segregando le discussioni in un ambiente chiuso che giustamente in molti rifiutano).

La piattaforma è già stata integrata con il sito [ils.org](https://www.ils.org/) (recentemente migrato da Drupal a Jekyll) per accogliere i commenti alle news qui pubblicate, e sono in corso test e perfezionamenti per dirottare il servizio di assistenza remota [annunciato un anno fa](https://www.ils.org/2020/06/04/help-desk.html) verso il forum pubblico, più inclusivo e cooperativo. Ma un poco alla volta il nuovo forum viene già utilizzato per altri annunci e discussioni, cui tutti possono partecipare [registrando un account](https://login.servizi.linux.it/module.php/core/frontpage_welcome.php) (che peraltro permette di accedere anche ad altri strumenti pubblici erogati da ILS, tra cui [la nostra raccolta di servizi web liberi e open source](https://servizi.linux.it/)).

È comunque importante notare che per questa nuova piattaforma è stato scelto [Discourse](https://www.discourse.org/), strumento già usato dalla [community internazionale di Mozilla](https://discourse.mozilla.org/) e da [Developers Italia](https://forum.italia.it/), che integra anche l'interazione via mail sia per la pubblicazione dei post che per l'invio di notifiche (secondo le proprie preferenze).
