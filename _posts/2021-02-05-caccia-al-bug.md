---
layout: post
title: Caccia al Bug
image: /assets/posts/images/bug.jpg
image_copy: Photo by <a href="https://unsplash.com/@tmillot?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Thomas Millot</a> on <a href="https://unsplash.com/s/photos/beetle?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a>
---

Nel 2020 Italian Linux Society non ha condotto campagne di raccolta fondi, e per recuperare a questa mancanza i soci hanno deciso di allocare 1000 dollari al sostegno economico dello sviluppo di codice libero e open source. È stato scelto di non limitarsi a delle semplici donazioni, ma di finanziare la risoluzione di specifici problemi che in qualche modo hanno toccato l'attività di ILS nel corso dell'anno per mezzo dello strumento delle "bounties": premi in denaro da assegnare a chi risolve l'implementazione desiderata.

<!--more-->

Qui di seguito l'elenco delle "taglie" piazzate su [Bountysource](https://www.bountysource.com/), ciascuna del valore di 200 dollari, che a loro volta contengono i link alle relative issues su GitHub coi dettagli operativi:

* [questa](https://www.bountysource.com/issues/37084263-enable-users-to-download-a-recording-as-video) e [questa](https://www.bountysource.com/issues/83209021-live-stream-a-session-via-rtmp): per integrare direttamente in [Big Blue Button](https://bigbluebutton.org/) le funzioni, rispettivamente, per il salvataggio dei video e per attivare lo streaming RTMP delle sessioni. Ovvero: includere direttamente nella piattaforma i comportamenti che sono stati in altro modo implementati in occasione del <a href="{% link _posts/2020-05-23-linux-day-2020.md %}">Linux Day Online 2020</a>

* [questa](https://www.bountysource.com/issues/91801688-support-single-sign-on-via-saml-or-openid-connect) per permettere l'autenticazione SAML2 in [Jitsi](https://jitsi.org/), nella prospettiva di estendere l'integrazione con [servizi.linux.it](https://servizi.linux.it)

* [questa](https://www.bountysource.com/issues/26880483-e-mail-notifications-daily-digest-for-channels) per avere un digest dei messaggi che transitano su [RocketChat](https://rocket.chat/), opzione essenziale per facilitare un reale utilizzo di [chat.linux.it](https://chat.linux.it) come canale di comunicazione

* [questa](https://www.bountysource.com/issues/74632356-allow-uploading-a-new-version-of-a-video) per permettere l'aggiornamento dei video su [PeerTube](https://joinpeertube.org/), problema che si è manifestato diverse volte in occasione del Linux Day

Ci auguriamo che questo piccolo contributo possano stimolare la crescita ed il miglioramento delle applicazioni in oggetto, utili sia all'interno della nostra associazione che a molti altri, e - se l'iniziativa avrà riscontro - di reiterarla anche nei prossimi anni.
