---
layout: post
title: Sicurezza.Linux.it
created: 1574790934
---
<p style="text-align: center">
  <img style="max-width: 100%" src="/assets/posts/images/sicurezza.png">
</p>

<p>
  Quello della sicurezza informatica è un tema di crescente interesse, data sia la pervasività delle tecnologie digitali che le loro implicazioni: sempre più dati, sempre più delicati e sensibili, sono conservati in archivi online cui potenzialmente possono accedere malintenzionati e truffatori che possono poi rivenderli o sfruttarli per azioni dannose.
</p>
<p>
  Il software libero e opensource gode di un certo vantaggio nell'identificazione e nella correzione di problemi relativi all'accesso a queste informazioni, ma gran parte degli sforzi sono orientati alle applicazioni più popolari e conosciute. I progetti minori non sempre possono trarre beneficio da una ampia community che contribuisca a revisionare e perfezionare il codice, e - benché spesso utilizzati per gestire dati personali - sono a volte esposti ad un maggiore rischio di violazione.
</p>
<p>
  Per questi motivi, e per un maggiore coinvolgimento attivo nell'ambito dello sviluppo opensource da parte dell'ampio pubblico che ha a cuore il tema, viene oggi lanciata l'iniziativa <a href="https://sicurezza.linux.it/">Sicurezza.Linux.it</a>.
</p>
<p>
  L'intento è quello di far incontrare gli sviluppatori con gli esperti (o aspiranti tali) di infosec, e far emergere problemi - e relative soluzioni - esistenti in progetti software meno noti ma non per questo meno sensibili ai rischi legati alla sicurezza. Periodicamente sarà proposto un nuovo progetto, con una opportuna istanza online su cui i white hat potranno sbizzarrirsi con test e prove e segnalare potenziali vulnerabilità, nonché ovviamente inviare le proprie patch e le proprie correzioni. Al termine di ogni campagna verrà redatto un report destinato agli sviluppatori del progetto, e sarà pubblicato l'elenco di coloro che maggiormente hanno contribuito a identificare, isolare, analizzare e correggere falle ed incongruenze.
</p>
<p>
  Per contribuire a rendere il software libero più sicuro, consulta il sito <a href="https://sicurezza.linux.it/">Sicurezza.Linux.it</a> e contribuisci con un pen test!
</p>
<p>
  Per maggiori informazioni, e per sottoporre nuovi progetti all'iniziativa, scrivi a <a href="mailto:webmaster@linux.it">webmaster@linux.it</a>.
</p>
