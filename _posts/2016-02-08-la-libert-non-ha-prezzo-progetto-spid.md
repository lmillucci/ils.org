---
layout: post
title: 'La Libertà non ha Prezzo: Progetto SPID'
created: 1454968762
---
Terza campagna di raccolta fondi su <a href="http://donazioni.linux.it/">donazioni.linux.it</a>, questa volta dedicata a tutta la pubblica amministrazione.

L'intento di questa nuova raccolta è quello di ottenere una implementazione libera ed opensource del protocollo <a rel="nofollow" href="http://www.agid.gov.it/agenda-digitale/infrastrutture-architetture/spid">SPID</a>, destinato ad unificare e semplificare l'accesso ai diversi servizi web esposti dai diversi enti pubblici per mezzo di un account unico per ogni cittadino. Tra gli obiettivi dell'iniziativa c'è quello di produrre e rilasciare plugins pronti all'uso per i più popolari CMS (Wordpress, Drupal e Joomla) in modo da accelerare il più possibile l'abilitazione al nuovo sistema di autenticazione da parte dei siti pubblici che già adottano, o vorranno adottare, piattaforme di pubblicazione libere.

Come sempre, anche in questa occasione vogliamo evidenziare alle istituzioni l'impatto che lo sviluppo condiviso e collaborativo ha e potrebbe avere nei confronti della digitalizzazione del nostro Paese, di cui tutti parlano ma che troppo spesso procede a rilento sul campo. Adottando il modello opensource per lo sviluppo e la pubblicazione delle componenti software ad uso della pubblica amministrazione i tempi ed i costi di adeguamento tecnologico da parte dei singoli enti - e soprattutto di quelli più piccoli e con minori disponibilità di competenze tecniche, come i piccoli comuni o le scuole periferiche - verrebbero immediatamente decimati.

Anche per il progetto SPID i soci di Italian Linux Society mettono a disposizione i primi 3000 euro, e tutte le quote delle nuove <a href="/iscrizione">iscrizioni all'associazione</a> che arriveranno nel corso della campagna saranno immediatamente destinate a questo fondo.

Su <a href="http://donazioni.linux.it/">donazioni.linux.it</a> è stata inoltre arricchita la pagina dei <a href="http://donazioni.linux.it/progetti/">progetti precedentemente finanziati</a> grazie al contributo di tutti, con dettagli sullo stato di avanzamento dei lavori che saranno via via aggiornati.
