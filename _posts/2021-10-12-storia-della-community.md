---
layout: post
title: La Storia della Community
image: /assets/posts/images/ghofoss.jpg
image_copy: Photo by <a href="https://unsplash.com/@geojango_maps?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">GeoJango Maps</a> on <a href="https://unsplash.com/s/photos/geography?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
---

Nell'anno in cui ricorrono i 30 anni di Linux, Italian Linux Society e Linux Professional Institute intendono mappare la storia della nostra community. Partendo dall'Italia, e poi nel resto del mondo.

<!--more-->

Il nostro Paese, come molti altri, è sede di numerosi gruppi e associazioni locali che conducono attività di promozione a Linux ed al software libero, ciascuno nella sua città, ciascuno con le sue persone e ciascuno con la sua propria storia. L'obiettivo del progetto **Glocal History of FOSS** #GHOFOSS, nato in seno a LPI e di cui ILS è stato il primo promotore, è quello di tracciare le milestone (cittadine, regionali e nazionali) che hanno accompagnato l'evoluzione della community a partire dal **25 agosto 1991**, ovvero dal primo annuncio pubblico di Linus Torvalds in merito al suo progetto amatoriale di kernel libero e aperto.

Per fare questo, ovviamente, abbiamo bisogno dell'**aiuto di tutti**. Sia per raccogliere le informazioni, di natura storica e geografica, che per rappresentarle poi nel modo più semplice e completo possibile.

<hr>

### Che cosa puoi fare. E quando

<br>

<img src="/assets/posts/images/maglietta.jpeg" class="float-right" style="width: 400px" />
**Da adesso al 22 ottobre**: inviaci, all'indirizzo [webmaster@linux.it](mailto:webmaster@linux.it), la tua proposta grafica per la navigazione della storia della community. Le informazioni sono di carattere geografico e storico, ed è dunque importante trovare una soluzione grafica che permetta sia di avere un quadro complessivo anno per anno che di esplorare i dettagli di una specifica area geografica. Quale può essere l'approccio migliore? Una mappa? Una timeline? Una griglia? Mandaci un mockup, un wireframe, un design, e aiutaci a visualizzare al meglio la storia della community: le tre migliori proposte riceveranno **una maglietta Linux.it**!

**Sabato 23 ottobre**: nel corso del [Linux Day Online](https://www.linuxday.it/2021/) verrà pubblicato un semplice form con il quale ciascuno potrà mandarci le sue proprie segnalazioni. Qualche esempio? La fondazione dello Users Group cittadino, il primo Linux Day, il primo corso... Raccontaci quello che è successo nella tua città in questi trent'anni di Linux! Il form rimarrà a disposizione anche dopo il Linux Day, perché la storia del software libero va avanti!

**Domenica 24 ottobre**: il giorno dopo il Linux Day ci incontreremo nuovamente, dalle 14:00 alle 20:00, per un hackathon online finalizzato ad implementare insieme il primo prototipo, partendo dai mockup e dalle informazioni raccolte nei giorni precedenti, prototipo che sarà poi esteso ed utilizzato per la Glocal History globale. Tutti i partecipanti riceveranno **un buono sconto del 30% per le certificazioni LPI**!

Seguici su [Twitter](https://twitter.com/ItaLinuxSociety), [Facebook](https://www.facebook.com/ItaLinuxSociety/) e [Mastodon](https://mastodon.uno/@ItaLinuxSociety) e partecipa al prossimo [Linux Day Online](https://www.linuxday.it/2021/) per maggiori informazioni, e consulta [questa pagina sul sito LPI](https://www.lpi.org/it/blog/2021/09/18/what-%E2%80%9Cglocal-history-foss%E2%80%9D-project-and-what-you-can-do-it) per saperne di più.
