---
layout: post
title: Piano Nazionale Innovazione 2025
created: 1576760448
---
<p style="text-align: center">
<img style="max-width: 100%" src="/assets/posts/images/italia2025.jpg">
</p>

<p>
Martedi 17 dicembre, presso il Tempo di Adriano a Roma, Italian Linux Society ha partecipato alla presentazione del Piano Nazionale Innovazione 2025, elaborato dalla Ministra Paola Pisano ed illustrato alla presenza del Premier Giuseppe Conte.<br>
Benché encomiabili siano l'impegno preso nei confronti della trasparenza (da attuare tramite la pubblicazione e l'aggiornamento di open data pubblici) e le misure pianificate per il sostegno all'imprenditoria italiana (spesso vessata dall'eccesso burocratico ed amministrativo), il documento pecca nell'identificare alcuni nodi strategici altresì già riconosciuti ed abbracciati nel resto d'Europa. Impossibile non notare il peso che all'interno del Piano Nazionale viene dato all'etica ed allo sviluppo inclusivo e sostenibile, considerati uno dei tre pilastri essenziali dell'intero apparato, ma altrettanto impossibile è non notare come non siano contemplati, in nessun modo ed in nessuna forma, temi di reiterato interesse economico, politico e sociale.
</p>

<p>
<strong>Privacy e tutela dei dati personali</strong>: già oggetto di diverse norme e regolamentazioni da parte della Commissione Europea, finalizzate ad arginare il quotidiano sopruso dei colossi digitali d'oltreoceano, non solo non trovano nessuno spazio nell'agenda del Ministero per l'Innovazione ma sono ulteriormente minacciate dalla cosiddetta "Identità Digitale", iniziativa orwelliana volta a centralizzare ancora più informazioni personali dei cittadini. Laddove SPID - il Sistema Pubblico di Identità Digitale, orientato a semplificare l'accesso degli utenti ai servizi della pubblica amministrazione - nasceva con un intento lodevole, tale percui la stessa <a href="{% link _posts/2016-02-08-la-libert-non-ha-prezzo-progetto-spid.md %}">Italian Linux Society ne ha sponsorizzato una implementazione libera</a> per i maggiori CMS open source, la nuova Identità Digitale estende il suo ambito di azione molto oltre i limiti di quel che sarebbe lecito ed auspicabile.
</p>
<p>
<strong>Impatto della spesa pubblica</strong>: il Piano prevede generosi fondi destinati al sostegno delle startup innovative, ma non da alcuna indicazione sul tipo di startup che saranno finanziate né sulle condizioni con cui tali fondi saranno erogati. Già dal 2013 la Francia attua un <a rel="nofollow" href="https://beta.gouv.fr/">programma per le "Startup di Stato"</a>, stimolando la creazione di nuovi servizi strumentali alla funzione pubblica e dunque al benessere dei cittadini, e tutto il software realizzato grazie ai finanziamenti statali così distribuiti viene <a rel="nofollow" href="https://github.com/betagouv">pubblicato con licenza open source</a> con l'evidente obiettivo di massimizzare e razionalizzare i benefici degli investimenti pubblici.
</p>
<p>
<strong>Scuola</strong>: anche se la Ministra Pisano, nel corso della presentazione, ha citato l'intento di portare il "coding" nelle aule, il Piano Nazionale lascia pochissimo margine per la scuola. Nessuna menzione alle risorse didattiche pubbliche e liberamente utilizzabili per l'insegnamento (che accelerano lo scambio di materiali, metodi e contenuti), nessuna suggestione sull'adozione di software libero e open source (che può essere legittimamente condiviso in classe tra gli studenti, per permetterne l'uso anche a casa senza incidere sulla spesa delle famiglie), nessun riconoscimento per le community spontanee di mutuo sostegno tecnologico e di auto-formazione digitale tra insegnanti (tra cui citiamo <a rel="nofollow" href="https://groups.google.com/forum/?hl=it#!forum/lavagnalibera">la mailing list Lavagna Libera</a>). Per approfondimenti su queste tematiche rimandiamo al portale <a href="http://scuola.linux.it/">Scuola.Linux.it</a>
</p>
<p>
<strong>Sovranità digitale</strong>: l'intera strategia attualmente operata da diversi Paesi europei per emanciparsi dalla dipendenza dei fornitori statunitensi di software e servizi viene liquidata in due righe di riferimento al <a rel="nofollow" href="https://www.agendadigitale.eu/infrastrutture/gaia-x-perche-e-importante-il-primo-cloud-su-scala-europea-litalia-ne-fara-parte/">progetto Gaia-X</a>, ed il Piano Nazionale non annuncia nessun ruolo destinato all'Italia in tale scenario. Germania e Francia hanno già iniziato a mettere in opera soluzioni in-house per l'hosting di documenti ed informazioni (<a rel="nofollow" href="https://nextcloud.com/fr_FR/blog/eu-governments-choose-independence-from-us-cloud-providers-with-nextcloud/">adottando, peraltro, la popolare piattaforma open source NextCloud</a>), sia per limitare l'accesso a tali documenti ai propri organi statali che per premiare, in modo oculato e lungimirante, l'offerta di servizi IT da parte delle proprie aziende locali; in Italia sono previsti data center ed infrastrutture cloud, ma non viene specificato quali saranno i fornitori che ne fruiranno per erogare i propri servizi.
</p>

<p>
In conclusione: il Piano, nell'essere estremamente ampio e variegato, non tocca i punti cardine essenziali che dovrebbero collocare l'Italia in un contesto europeo - attento alla privacy, alla condivisione ed alla collaborazione - che gli sarebbe proprio. L'attività condotta negli ultimi anni da parte del Team Digitale, sul fronte dell'implementazione dei servizi di base e della spinta verso l'interoperabilità e la razionalizzazione dei costi anche per mezzo del modello open source, non può dirsi conclusa né sembra trovare pieno riscontro nella direzione presa dal nuovo Ministero per l'Innovazione.<br>
Auspichiamo che siano comunque presi in considerazione anche questi fattori in fase di attuazione effettiva del Piano, per il bene dei cittadini, delle imprese, delle pubbliche amministrazioni e delle scuole del nostro Paese.
</p>

<p>
Il testo completo del Piano Nazionale <a rel="nofollow" href="https://innovazione.gov.it/assets/docs/MID_Book_2025.pdf">è reperibile qui</a>, e qua si trova <a rel="nofollow" href="https://www.youtube.com/watch?v=6IY7agL5p-4">la registrazione audio/video della conferenza di presentazione</a>.
</p>
