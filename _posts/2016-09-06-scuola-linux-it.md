---
layout: post
title: Scuola.Linux.it
created: 1473115294
---
<img src="/assets/posts/images/scuolalinux.png" style="margin: auto; display: block" />

<br/>

<p>
Quale miglior giorno di quello della riapertura delle scuole per mettere online il nuovo <a href="http://scuola.linux.it/">scuola.linux.it</a>?! Lo storico dominio, esistente sin dal 2000, ha negli anni subito alcuni rimaneggiamenti sino a diventare un alias verso <a rel="nofollow" href="http://linuxdidattica.org/">linuxdidattica.org</a>, nutrita raccolta di testi sia didattici che tecnici sull'adozione del software libero a scuola (a tutt'ora raggiungibile online).
</p>

<p>
Date le progressive evoluzioni della community promotrice non solo del software libero ma più in generale delle libertà digitali in contesto scolastico, e la nascita di numerosi gruppi tematici, indici e cataloghi online, strumenti e piattaforme, abbiamo pensato di riadattare scuola.linux.it per fornire un conciso ma funzionale elenco di spunti e links dedicati a chi vuole saperne di più sul tema. Esattamente come <a href="http://www.linux.it/">www.linux.it</a> accoglie e guida ogni giorno centinaia di visitatori verso gli approfondimenti desiderati e le community locali, il nuovo scuola.linux.it intende essere un punto di partenza per chi vuole saperne e capirne di più in merito a informatizzazione della didattica, digitalizzazione del sapere e tecnologie a scuola, anche e soprattutto partecipando (si spera, attivamente) alle principali <a href="http://scuola.linux.it/community">community</a> già esistenti.
</p>

<p>
Sul sito saranno periodicamente riportate le notizie più rilevanti intercettate appunto sulle diverse mailing lists attive, e verrà usato per dare maggiore visibilità alle iniziative dei nostri amici insegnanti.
</p>
