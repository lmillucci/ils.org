---
layout: post
title: 'LUG: Manuale Operativo'
created: 1377137220
---
Tutti sanno che gran parte delle risorse di <a href="/">Italian Linux Society</a> sono destinate al supporto della community freesoftware italiana, ma non tutti conoscono nel dettaglio i servizi offerti sia ai gruppi che aderiscono all'associazione sia a tutti coloro che densamente popolano il Paese portando ovunque, dai piccoli centri alle grandi citta', la voce del software libero.

Pertanto in queste settimane e' stato redatto il "Manuale Operativo per la Community", reperibile <a href="/progetti#servizi">nella sezione "Progetti" di questo sito</a>. Un breve documento che elenca le opportunita' offerte e fornisce maggiori istruzioni e chiarimenti su come accedervi: cosa e' riservato ai soci e cosa e' disponibile a tutti, chi contattare di caso in caso, quali sono le procedure... Un manuale, appunto, dai contenuti sintetici e focalizzati sugli aspetti pratici e pragmatici.

L'obiettivo dell'iniziativa e' certamente quello di mettere in luce le possibilita' esistenti, ma anche di incentivare sempre piu' Linux Users Groups ed associazioni affini - e non solo le singole persone - ad associarsi direttamente ad ILS al fine di aggregare una massa critica sempre piu' grande e con un peso rappresentativo sempre maggiore da far valere poi nei confronti di istituzioni, enti pubblici e privati, fondazioni, altre associazioni, ed insomma tutti i soggetti di portata nazionale che potrebbero (e dovrebbero) interessarsi al tema della tecnologia libera, trasparente, condivisa e accessibile.

Il "Manuale Operativo" e' destinato ad arricchirsi e crescere nel tempo, sia per presentare i nuovi servizi che verranno erogati da ILS che con dritte e suggerimenti per chi intende costituire un nuovo LUG nella propria citta' partendo da zero: i gruppi a sostegno del software libero <a href="http://lugmap.linux.it/">sono tanti</a> ma mai abbastanza, e nuove partecipazioni sono sempre estremamente gradite e benvenute.
