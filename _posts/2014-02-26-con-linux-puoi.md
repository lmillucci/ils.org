---
layout: post
title: Con Linux Puoi
created: 1393378574
---
Che Linux sia un sistema adatto alla maggior parte degli utilizzi casalinghi lo sanno pressoché tutti coloro che abitualmente lo adoperano con soddisfazione sui loro rispettivi PC. Ma ancora in molti non lo sanno, soprattutto tra coloro che più ne sarebbero interessati: chi utilizza Windows XP, piattaforma rilasante oramai a più di 10 anni fa per la quale <a rel="nofollow" href="http://www.microsoft.com/it-it/windows/business/retiring-xp.aspx">nel giro di un mese verrà definitivamente sospeso il supporto da parte di Microsoft</a> e che dunque non riceverà più aggiornamenti atti a garantirne (o quantomeno a tamponarne) la stabilità e la sicurezza, rendendolo ancor più vulnerabile ad ogni genere di crash o attacco informatico.

Pertanto vi invitiamo a far conoscere l'opportunità offerta dal software libero e opensource ad amici e conoscenti per mezzo dell'iniziativa "Con Linux Puoi": una semplice pagina web che riassume i validi motivi per preferire Linux e alcuni riferimenti utili da condividere sul web, sui social network, sui blog, magari accompagnandolo con un proprio commento basato sulla propria esperienza.

Cosa si può fare con Linux? Navigare il web in modo veloce e protetto. Mantenere sempre aggiornato il proprio sistema grazie agli strumenti integrati per l'amministrazione delle applicazioni. Creare documenti, video, immagini e musica sfruttando l'ampio insieme di programmi a disposizione. E, prerogativa assoluta di Linux rispetto a tutti gli altri: avere completo accesso al funzionamento dell'intero struttura, e dunque avere la possibilità di capirne i meccanismi e modificarlo in ogni aspetto.

Il resto, diccelo tu!
