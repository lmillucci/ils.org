---
layout: post
title: 2019 con ILS
created: 1577048559
---
Come sempre, con la fine dell'anno, proponiamo una panoramica delle attività che hanno vista coinvolta Italian Linux Society negli ultimi 12 mesi. Nonché qualche anticipazione per l'inizio del 2020.

<ul>
    <li>anche quest'anno siamo stati presenti a diversi eventi: all'<a rel="nofollow" href="https://www.instagram.com/p/B2q-G96ndZW/">End Summer Camp di Venezia</a> e alla <a rel="nofollow" href="https://www.sfscon.it/">South Tyrol Free Software Conference di Bolzano</a>, per incontrare e confrontarci con le community presenti, e al <a rel="nofollow" href="https://www.instagram.com/p/B0akvWUn_Ce/">Campus Party di Milano</a>, per portare Linux ed il software libero al variegato pubblico della manifestazione</li>
    <li>più recentemente abbiamo partecipato alla presentazione del Piano Nazionale Innovazione, svolta a Roma da parte del <a rel="nofollow" href="https://innovazione.gov.it/">Ministero dell'Innovazione</a>, e <a href="{% link _posts/2019-12-19-piano-nazionale-innovazione-2025.md %}">ne abbiamo tratto qualche considerazione</a> che orienterà almeno in parte l'attività nel corso del prossimo anno</li>
    <li>abbiamo destinato circa 2000 euro (di cui 1000 donati direttamente da ILS, e 1000 raccolti sulla nostra <a href="http://donazioni.linux.it/">piattaforma per il crowdfunding</a>) al <a rel="nofollow" href="https://www.simcaa.it/">progetto SIMCAA</a>, applicazione (libera, ovviamente) per semplificare la comprensione dei testi con l'ausilio di immagini</li>
    <li>abbiamo inaugurato <a href="{% link _posts/2019-11-26-sicurezza-linux-it.md %}">Sicurezza.Linux.it</a>, nuova iniziativa destinata ad incrementare la sicurezza dei progetti open source più piccoli e a coinvolgere in modo più attivo la community nella produzione e nella revisione del software libero</li>
    <li>le Sezioni Locali ILS, protagoniste del prossimo riassetto interno dell'associazione e finalizzate a rendere più facile la vita dei gruppi locali di promozione a Linux, sono state <a href="{% link _posts/2019-10-30-sezioni-locali-prima-bozza.md %}">oggetto di discussione</a> e già da gennaio verranno formalmente inaugurate le prime</li>
    <li>a partire da quest'anno abbiamo iniziato a <a href="{% link _posts/2019-05-21-dillo-con-uno-sticker.md %}">spedire adesivi promozionali a coloro che ne fanno richiesta</a>, e già abbiamo inviato decine di buste in tutta Italia</li>
    <li>immancabilmente a ottobre si è tenuto <a href="https://www.linuxday.it/2019/">il Linux Day</a>, la maggiore manifestazione nazionale dedicata al software libero coordinata da ILS. E vi ricordiamo che nel 2020 ricorre il secondo decennale!</li>
    <li>abbiamo appena annunciato l'edizione 2020 del <a rel="nofollow" href="https://fosdem.linux.it/">FOSDEM Extended</a>, che si terrà l'1 ed il 2 febbraio, con l'auspicio di coinvolgere maggiormente le aziende che lavorano con strumenti liberi e open source</li>
</ul>

Per restare sempre aggiornati su ciò che succede nel mondo del software libero in Italia potete sottoscrivere <a href="/newsletter">la nostra newsletter</a> o seguirci <a rel="nofollow" href="https://twitter.com/ItaLinuxSociety/">su Twitter</a>, <a rel="nofollow" href="https://www.facebook.com/italinuxsociety/">su Facebook</a> e <a rel="nofollow" href="https://www.instagram.com/italinuxsociety/">su Instagram</a>. Nonché associarvi alla nostra associazione e godere di <a href="/iscrizione">tutti i benefici riservati ai soci ILS</a>.
