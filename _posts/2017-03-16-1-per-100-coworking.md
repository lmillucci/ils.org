---
layout: post
title: 1% per 100 Coworking
created: 1489629740
---
<p style="text-align: center">
<img src="/assets/posts/images/flyers_1percento.jpg" alt="1%">
</p>

<p>
Dopo aver raccolto diversi feedback positivi in merito all'<a href="{% link _posts/2017-01-10-1.md %}">iniziativa 1%</a>, rivolta alla sensibilizzazione dei tanti professionisti che lavorano - più o meno consapevolmente - con software libero e opensource nei confronti del ruolo che possono e devono avere per sostenere economicamente i propri stessi strumenti produttivi, passiamo ora alla fase successiva.
</p>

<p>
In questi giorni stiamo inviando una lettera a 100 coworking distribuiti in tutta Italia, con un paio di copie del volantino divulgativo e qualche adesivo da distribuire a coloro che frequentano la struttura. L'intento è quello di allargare il pubblico raggiunto dall'appello, coinvolgendolo laddove è più facile trovarlo, proprio nei luoghi dove si svolgono le attività lavorative e, magari davanti alla macchinetta del caffé, si possa discutere dell'argomento con altre persone più o meno direttamente interessate. In questo modo vogliamo toccare una parte sempre più ampia degli operatori, anche e soprattutto al fuori dei "soliti" canali di comunicazione prettamente dedicati all'opensource, ed ispirare una porzione sempre maggiore di professionisti a prendere parte attiva nel processo di svluppo e sostegno del bene comune digitale.
</p>

<p>
Tra le altre novità correlate all'iniziativa segnaliamo che la pagina web è stata <a href="http://www.linux.it/unopercento/en">tradotta in inglese</a>, al fine di allargare lo spunto al di fuori dei confini nazionali ed indurre gli altri promotori freesoftware europei a svolgere campagne analoghe nei rispettivi Paesi.
</p>
