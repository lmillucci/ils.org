---
layout: post
title: European Software Freedom Policy Meeting 2017
created: 1486507508
---
Anche quest'anno, sulla scia del <a href="{% link _posts/2016-02-03-european-software-freedom-policy-meeting.md %}">primo incontro del 2016</a> e con l'occasione del <a rel="nofollow" href="https://fosdem.org/2017/">FOSDEM 2017</a>, venerdi 3 febbraio si è svolto a Bruxelles l'European Software Freedom Policy Meeting, organizzato da <a rel="nofollow" href="https://fsfe.org/">Free Software Foundation Europe</a> e <a rel="nofollow" href="http://www.openforumeurope.org/">Open Forum Europe</a> con l'intento di aggiornare reciprocamente le diverse realtà operative nel Vecchio Continente sul fronte delle libertà digitali. Naturalmente Italian Linux Society non ha mancato di partecipare anche in questa occasione.

All'ordine del giorno i prossimi sviluppi dell'iniziativa <a rel="nofollow" href="https://joinup.ec.europa.eu/community/eu-fossa/home">EU-FOSSA</a> - con la quale la Commissione Europea finanzia attività di auditing su progetti opensource -, il <a rel="nofollow" href="https://github.com/DISIC/foss-contrib-policy-template">FOSS Contribution Policy Template</a> - documento rivolto a formalizzare la partecipazione delle amministrazioni statali a progetti di sviluppo -, e una discussione sul monitoraggio del rispetto dei principi di apertura e trasparenza digitale da parte dei diversi Paesi comunitari.

Come in ogni meeting che si rispetti un ruolo determinante hanno avuto anche le pause caffé, sfruttate per scambiare due chiacchere con altri promotori del software libero ed opensource, avere una più chiara idea di cosa succede negli altri Paesi, e raccogliere pareri sulle prossime iniziative da portare qui in Italia.
